
import math

from django.db import models
from django.db.models.signals import post_save, pre_save, pre_delete
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import UserManager as BaseUserManager
from django.core.files.storage import default_storage
from django.core.mail import send_mail
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from rest_framework.authtoken.models import Token

from restAPI import choices
from restAPI import imtools


class UserManager(BaseUserManager):
    """
    UserManager subclasses the default django UserManager class to handle
    user account creation
    """

    def create_user(self,
                    username=None,
                    email=None,
                    password=None,
                    **extra_fields):
        """
        Create a new `User` instance using the supplied email address as
        the USERNAME_FIELD, and ignoring the `username` argument. The supplied
        `username` argument is only included in the method prototype so that
        the manager is compatible with forms and views that do not correctly
        implement the Django v1.5+ `User` model and require a username field.
        """
        if not email:
            raise ValueError('Accounts require an email address.')
        email = self.normalize(email)  # force email addresses lowercase
        user = self.model(email=email,
                          is_staff=False,
                          is_active=True,
                          is_superuser=False)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        """Create a new `User` and set admin permissions."""
        su = self.create_user(email=email,
                              password=password,
                              **extra_fields)
        su.is_staff = True
        su.is_active = True
        su.is_superuser = True
        su.save(using=self._db)
        return su

    def normalize(self, field):
        """Force strings to lowercase & strip leading/trailing whitespace."""
        return field.lower().strip()


class User(AbstractBaseUser, PermissionsMixin):
    """
    `User` extends the default Django `User` model to use an email address as
    account username and add app-specific fields.

    https://geekwentfreak-raviteja.rhcloud.com/2012/
        12/custom-user-models-in-django-1-5/

    Custom User objects are not completely supported by the django-regsitration
    v1.0 module `default` backend implementation.
    """
    # Set dynamic username lookup field
    USERNAME_FIELD = 'email'

    # Link to the model handler that should be used as the interface to the db
    objects = UserManager()

    email = models.EmailField(_('E-mail Address'),
                              max_length=254,
                              unique=True,
                              )
    is_staff = models.BooleanField(_('Staff'),
                                   default=False,
                                   help_text="Designates whether the user has \
                                             admin privileges.",
                                   )
    is_active = models.BooleanField(_('Active Status'),
                                    default=True,
                                    )
    date_joined = models.DateTimeField(_('Date Joined'),
                                       auto_now_add=True,
                                       )
    picture = models.ImageField(_('Profile Picture'),
                                upload_to='images/users/profile/',
                                null=True,
                                blank=True,
                                )
    mailing_list = models.BooleanField(_('Receive E-mail Updates'),
                                       default=True,
                                       )

    def _set_username(self):
        return getattr(self, User.USERNAME_FIELD)

    def _get_username(self, value):
        pass

    # Adding a false `username` field ensures compatibility with apps that
    # expect a default Django `User` model instance
    username = property(_get_username, _set_username)

    # Required for permissions
    def get_full_name(self):
        return self.email

    # Required for permissions
    def get_short_name(self):
        name_parts = self.email.partition('@')
        return name_parts[0]

    # Required for sending actiavtion email
    def email_user(self, subject, message, from_email=None):
        """Initiates sending an email to the specified user."""
        send_mail(subject, message, from_email, [self.email])


@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    """
    Automatically generate authentication tokens for each new user added.
    http://django-rest-framework.org/
        api-guide/authentication.html#tokenauthentication
    """
    if created:
        Token.objects.create(user=instance)


class Wine(models.Model):
    """
    The `Wine` model represents individual wines that have been added to the
    app.

    Only staff/admin users can add new wines, although any authenticated user
    can vote for or against a wine by creating a `Review`. The images are
    stored in an Amazon S3 bucket defined in the settings.py file of the main
    app.
    """
    REL_PATH = 'images/wines/'
    WIDTH_LIST = (100, 200, )
    date_added = models.DateTimeField(_("Date added"),
                                      auto_now_add=True,
                                      )
    name = models.CharField(_("Name"),
                            max_length=150,
                            blank=False,
                            )
    region = models.CharField(_("Region"),
                              max_length=150,
                              blank=False,
                              )
    year = models.CharField(_("Year"),
                            max_length=150,
                            blank=False,
                            )
    kind = models.CharField(_("Wine Type"),
                            max_length=50,
                            blank=False,
                            choices=choices.TYPE_LIST,
                            default=choices.TYPE_LIST[0][0],
                            )
    score = models.FloatField(_("Average Rating"),
                              default=0,
                              blank=False,
                              )
    picture = models.ImageField(upload_to=REL_PATH)
    # thumbnail = models.ImageField(upload_to='thumbnails/wines/')

    @property
    def image_filenames(self):
        """
        Return a list of filenames expected to exist in the `default_storage`
        location that are associated with the `picture` field of this
        instance by calling the filename generator expression.
        """
        return list(self._filenames())

    def _filenames(self):
        """
        Return a generator of the file names and path for all image files that
        should be stored in the `default_storage` location for this instance.
        """
        filename = self.picture.name
        if not default_storage.exists(filename):
            filename = self.picture.path  # DEBUG uses absolute path
        yield filename
        name, _dot, ext = filename.rpartition('.')
        for width in self.WIDTH_LIST:
            yield name + "_" + str(width) + _dot + ext


@receiver(pre_save, sender=Wine)
def create_thumbnails(sender, instance=None, **kwargs):
    """
    Automatically resize wine label images added to the database (stored on
    s3) by queuing a task (using rq on Heroku).

    https://devcenter.heroku.com/articles/python-rq
    """
    if not instance.picture:
        return
    ext = instance.picture.name.rpartition('.')[2]
    uuid = imtools.random8_uuid()
    instance.picture.name = uuid + '.' + ext
    # schedule image resizing https://devcenter.heroku.com/articles/python-rq
    # assumed format of enqueue(callback_fn, *positional_args)
    # enqueue(imtools.batch_resize,
    #         instance.picture,
    #         Wine.REL_PATH,
    #         Wine.WIDTH_LIST)


@receiver(pre_delete, sender=Wine)
def cleanup_thumbnails(sender, instance=None, **kwargs):
    """
    Automatically remove files from the s3 bucket when the associated wine has
    been removed from the database by queuing a task (using rq on Heroku).
    """
    if not instance.picture:
        return
    # schedule image cleanup https://devcenter.heroku.com/articles/python-rq
    # assumed format of enqueue(callback_fn, *positional_args)
    # enqueue(imtools.cleanup, instance.image_filenames)


class Review(models.Model):
    """
    The `Review` model connects the `User` model to the `Wines` model.

    The limited `text` field is intended to be used with twitter integration.
    """

    # define score label constants
    LIKE = 1
    DISKLIKE = -1
    SCORES = (
        (LIKE, "Like"),
        (DISKLIKE, "Dislike"),
    )

    user = models.ForeignKey(User)
    wine = models.ForeignKey(Wine, related_name='reviews')
    date_added = models.DateTimeField(_("Date added"),
                                      auto_now_add=True,
                                      )
    score = models.IntegerField(_("Rating"),
                                choices=SCORES,
                                blank=False,
                                )
    text = models.CharField(_("Review"),
                            max_length=140,
                            blank=False,
                            )

    class Meta:
        # Django does not support compound keys; instead use the
        # unique_together constraint in the Meta-class:
        # http://stackoverflow.com/questions/
        #   2201598/how-to-define-two-fields-unique-as-couple
        unique_together = ('user', 'wine')


@receiver(post_save, sender=Review)
def update_score(sender, instance, created=False, **kwargs):
    """
    Update the average rating field of a `Wine` after a new `Review` is
    created - this method is not valid for "stars" (look at bayesian average
    instead: http://fulmicoton.com/posts/bayesian_rating/)

    The score is calculated using the lower bound of the Wilson score for
    binomial distribution as seen here:
    http://www.evanmiller.org/how-not-to-sort-by-average-rating.html

    The version of the Wilson score used below is algebraically equivalent
    to the standard form; it is refactored here to inline the expressions.

    This implementation uses a constant z-score, z=1.96, calculated from the
    Standard Normal Distribution for 95% confidence interval; other values
    can be found on a standard normal distribution z-table, or can be
    calculated using the scipy stat package.
    """

    if created:
        # Get affected `Wine` object and *all* associated reviews
        wine = Wine.objects.get(pk=instance.wine.pk)
        reviews = Review.objects.filter(wine=instance.wine.pk)

        # count the total reviews, positive reviews, and their ratio
        n = reviews.count()
        pos_n = reviews.filter(score__gt=0).count()
        p_hat = float(pos_n / n)

        # square of z-score for 95% CI
        z_sq = 3.8416

        # Calculate the lower bound of the Wilson score
        num = (p_hat * z_sq) - math.sqrt(z_sq + (4 * (1 - p_hat) * pos_n))
        den = 2 * (z_sq + n)
        wine.score = num / den

        # Save the result to the database
        wine.save(update_fields=['score'])
