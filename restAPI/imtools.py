
import StringIO
import uuid

import os
from os import SEEK_END
from PIL import Image

from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.core.files.uploadedfile import InMemoryUploadedFile


def random8_uuid():
    """
    Generate an 8-digit ID from a UUID to use as a filename.
    """
    return str(uuid.uuid4())[:8]


def get_size(s):
    """
    Get the size of a StringIO file object by seeking to the end of the file
    and reporting the location.
    """
    s.seek(0, SEEK_END)
    return s.tell()


def calculate_height(img_size, new_width):
    """
    Calculate proportional height for defined-width scaling of an image.
    """
    wpercent = float(new_width)/img_size[0]
    new_height = int(float(img_size[1]) * wpercent)
    return new_height


def cleanup(image_files):
    """
    `cleanup` is a callback function to be used in conjuction with a task
    queue (https://devcenter.heroku.com/articles/python-rq) to remove unused
    files after a wine instance is deleted from the database.
    """
    for filename in image_files:
        default_storage.delete(filename)


def batch_resize(picture, path, width_list):
    """
    `batch_resize` is a callback function to be used conjuction with a task
    queue (https://devcenter.heroku.com/articles/python-rq) to automatically
    create multiple scaled versions of wine label images uploaded with new
    instances of `Wine`.
    """
    file_obj = StringIO.StringIO()
    uuid, _, ext = picture.name.rpartition('.')
    pil_image = Image.open(ContentFile(picture.file.read()))

    for width in width_list:
        filename = "{0}_{1!s}.{2}".format(uuid, width, ext)
        height = calculate_height(pil_image.size, width)

        # Create resized image and convert to StringIO format
        im = pil_image.resize((width, height), Image.ANTIALIAS)
        im.save(file_obj, pil_image.format)

        # Convert to django file type uploaded with form fields & save
        im_file = InMemoryUploadedFile(file_obj,
                                       None,
                                       filename,
                                       picture.file.content_type,
                                       get_size(file_obj),
                                       None)
        default_storage.save(os.path.join(path, filename),
                             im_file)


def make_thumbnail(fileobj):
    """
    Given a filefield object (a file that has been uploaded from a form field
    in Django), auto_thumbnail converts the file and opens it with the Python
    Image Library and makes a thumbnail version, then returns it as an uploaded
    file object (for use in an imagefield in a db).
    """
    THUMBNAIL_SIZE = 200, 200
    pil_image = Image.open(ContentFile(fileobj.read()))
    pil_image.thumbnail(THUMBNAIL_SIZE, Image.ANTIALIAS)
    thumb_fileobj = StringIO.StringIO()
    pil_image.save(thumb_fileobj, pil_image.format)
    thumb_size = get_size(thumb_fileobj)
    return thumb_fileobj, thumb_size
