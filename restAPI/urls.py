from django.conf.urls import patterns, url
from restAPI import views

# TODO: https://docs.djangoproject.com/en/dev/topics/http/urls/#named-groups
# Use named groups to parameterize queries on the view sets (test to see if
# it applies all parameters as filters, e.g.) to search for things like "color",
# etc.

urlpatterns = patterns('',
    url(r'^reviews/$', views.ReviewList.as_view()),
    url(r'^users/$', views.UserList.as_view()),
    url(r'^wines/$', views.WineList.as_view()),
    url(r'^wines/(?P<pk>[0-9]+)$', views.WineDetail.as_view()),
    url(r'^suggested/$', views.SuggestedWineList.as_view()),
    url(r'^api-token-auth/',
        'rest_framework.authtoken.views.obtain_auth_token'),
)