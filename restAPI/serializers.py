"""
Automatically generate serializers from defined model schemas using
django-rest-framework.

http://django-rest-framework.org/api-guide/serializers.html#modelserializer

"""

from rest_framework import serializers
from restAPI.models import User, Review, Wine


class UserSerializer(serializers.ModelSerializer):
    # Include the `auth_token` from django-registration app in the `User`
    # model serializer
    auth_token = serializers.SlugRelatedField(many=False,
                                              read_only=True,
                                              slug_field='key'
                                              )

    class Meta:
        model = User
        fields = ('email', 'date_joined', 'auth_token', 'picture',)
        read_only_fields = fields


class WineSerializer(serializers.ModelSerializer):
    # Field name must be equal to the `related_name` on the foreign key
    # associating the `Wine` model with the `Review` model.
    reviews = serializers.RelatedField(many=True)

    class Meta:
        model = Wine
        fields = ('name', 'year', 'kind', 'region', 'picture',)
        read_only_fields = ('id', 'date_added',)


class ReviewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Review
        read_only_fields = ('user', 'date_added',)
