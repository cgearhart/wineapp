
# Define the list of wines types that are available to be chosen for new
# `Wine` instances.

TYPE_LIST = (
    (None, (
            (None, 'Choose a type...'),
        )
    ),
    ('Red', (
            ('merlot', 'Merlot'),
        )
    ),
    ('White', (
            ('savignon blanc', 'Savignon Blanc'),
        )
    ),
    ('Rose', (
            ('white zinfandel', 'White Zinfandel'),
        )
    ),
    ('Sparkling', (
            ('moscato', 'Moscato'),
        )
    ),
    ('Fortified', (
            ('ruby port', 'Ruby Port'),
        )
    ),
)
