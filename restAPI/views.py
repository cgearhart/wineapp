
from rest_framework import generics
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.authentication import (TokenAuthentication,
                                           SessionAuthentication)

from restAPI.models import User, Wine, Review
from restAPI.serializers import (UserSerializer,
                                 WineSerializer,
                                 ReviewSerializer)


class IsStaffOrReadOnly(permissions.BasePermission):
    """
    Object-level permission to only allow staff users to edit them. Assumes
    the `request.user` instance has an `is_staff` attribute.
    """
    def has_permission(self, request, view):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        return request.user.is_staff


def owner_queryset(model, key, value):
    """
    Build a queryset that filters results for those owned by the currently
    authenticated user.
    """
    def get_queryset(self):
        val = reduce(getattr, value.split('.'), self)
        return model.objects.filter(**{key: val})
    return get_queryset


class WineList(generics.ListCreateAPIView):
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsStaffOrReadOnly,)  # must be staff user to add wine
    queryset = Wine.objects.all()
    serializer_class = WineSerializer
    paginate_by = 10
    paginate_by_param = "recent"


class WineDetail(generics.RetrieveAPIView):
    queryset = Wine.objects.all()
    serializer_class = WineSerializer


class SuggestedWineList(generics.ListAPIView):
    queryset = Wine.objects.order_by('-score')
    serializer_class = WineSerializer
    paginate_by = 10
    paginate_by_param = "top"


class UserList(generics.ListAPIView):
    # List view does not handle creating users
    serializer_class = UserSerializer
    get_queryset = owner_queryset(User, 'pk', 'request.user.pk')


class ReviewList(generics.ListCreateAPIView):
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticatedOrReadOnly, )
    serializer_class = ReviewSerializer
    get_queryset = owner_queryset(Review, 'user', 'request.user.pk')

    def pre_save(self, obj):
        """
        Automatically use the current user as the user field for new reviews.
        """
        obj.user = self.request.user
